# @b08/memoize, seeded from @b08/library-seed, library type: feature
implementation of memoization pattern

# memoize
It uses arguments as a cache key. So next time the function is called with the same parameters, value will be taken from cache, unless expired.
```
const calculateItems = memoize((arg1: string, arg2: number) => {
  // heavy calculations go here
});

// to reset the cache
calculateItems.reset();
```

There is a restriction - object parameters should be immutable, since parameters are compared using reference comparison.

# memoized
This has the same functionality as memoize, but uses serialization of arguments instead of direct comparison.
