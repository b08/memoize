import { KeyMap } from "@b08/flat-key";

export interface MemoizedFunc<TP extends any[], TR> {
  (...p: TP): TR;
  reset: () => void;
}

export const memoize = function memoize<TP extends any[], TR>(func: (...p: TP) => TR): MemoizedFunc<TP, TR> {
  const cache: KeyMap<any, any>[] = [];
  const result: any = function (this: any, ...args: any[]): any {
    let i = arguments.length;
    const map = cache[i] || (cache[i] = new KeyMap());
    const key = Object.create(null);
    while (i--) { key[i] = arguments[i]; }
    if (map.has(key)) { return map.get(key); }
    const value = func.apply(this, args);
    map.add(key, value);
    return value;
  };
  result.reset = () => cache.length = 0;
  return result;
};

export function memoized<TP extends any[], TR>(func: (...p: TP) => TR): MemoizedFunc<TP, TR> {
  const cache = new Map();
  const result: any = function (this: any, ...args: any[]): any {
    const key = JSON.stringify(args);
    if (cache.has(key)) { return cache.get(key); }
    const value = func.apply(this, args);
    cache.set(key, value);
    return value;
  };
  result.reset = () => cache.clear();
  return result;
}
