import { test } from "@b08/test-runner";
import { memoize } from "../src";

const calls = [];
function calledFunc(arg1: number, arg2: string): number {
  calls.push({ arg1, arg2 });
  return arg1 + +arg2;
}

test("memoize should reset cache", async expect => {
  // arrange
  calls.length = 0;
  const target = memoize(calledFunc);
  const arg1 = 1, arg2 = "2";
  const expected = [{ arg1, arg2 }, { arg1, arg2 }];
  // act
  target(arg1, arg2);
  target.reset();
  target(arg1, arg2);

  // assert
  expect.deepEqual(calls, expected);
});
