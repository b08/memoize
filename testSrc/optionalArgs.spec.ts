import { describe } from "@b08/test-runner";
import { memoize } from "../src";

const calls = [];

function calledFunc(arg1: number, arg2?: string): number {
  calls.push({ arg1, arg2 });
  return arg1 + (arg2 ? +arg2 : 0);
}

describe("memoize", it => {
  it("should simultaneously work with different args length", expect => {
    // arrange
    calls.length = 0;
    const target = memoize(calledFunc);

    const expected = [{ arg1: 2, arg2: undefined }, { arg1: 2, arg2: "2" }];

    // act
    target(2);
    target(2, "2");

    expect.deepEqual(calls, expected);
    const result = target(2, "2");
    const result2 = target(2);

    // assert
    expect.deepEqual(calls, expected);
    expect.equal(result, 4);
    expect.equal(result2, 2);
  });
});
