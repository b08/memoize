import { describe } from "@b08/test-runner";
import { memoize } from "../src";

const calls = [];

function calledFunc(arg1: number, arg2: string): number {
  calls.push({ arg1, arg2 });
  return arg1 + +arg2;
}

describe("memoize", it => {
  it("should memoize the function result", expect => {
    // arrange
    calls.length = 0;
    const target = memoize(calledFunc);
    const arg1 = 1, arg2 = "2";
    const expected = [{ arg1, arg2 }];
    // act
    target(arg1, arg2);
    expect.deepEqual(calls, expected);
    const result = target(arg1, arg2);

    // assert
    expect.deepEqual(calls, expected);
    expect.equal(result, 3);
  });

  it("should memoize 2 function results", expect => {
    // arrange
    calls.length = 0;
    const target = memoize(calledFunc);

    const arg1 = 1, arg2 = "2";
    const arg11 = 3, arg12 = "4";
    const expected = [{ arg1, arg2 }, { arg1: arg11, arg2: arg12 }];

    // act
    target(arg1, arg2);
    target(arg1, arg2);
    target(arg11, arg12);
    target(arg11, arg12);

    // assert
    expect.deepEqual(calls, expected);
  });
});
